Author: Zahra Jalili  
All Rights Reserved.  
February 2020.   
----------------------------------------------------------------  
This project plots different visualizations for investigating the effect  
of smoking on heart disease and storke in all states of US from 2011 to 2015.  
It also proves that smoking has worst effect on females than males.  
-----------------------------------------------------------------  

## Install:  
In order to be able to use this software please download the following libraries.   
1. numpy  
2. pandas  
3. seaborn  
4. statsmodels  
5. scipy  
6. matplotlib  
7. PIL  
8. win32api  
  
  
After installing the aforementioned libraries you can easily run the app.